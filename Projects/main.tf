
provider "google" {
    project = "your-gcp-project-id"
    region  = "your-gcp-region"
    credentials = file("path/to/your/service-account-key.json")
}