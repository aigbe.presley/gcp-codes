provider "google" {
    project = "august-copilot-197319"
    region  = "your-gcp-region"
    credentials = file("key.json")
}